import UpdateCommandForm from "components/UpdateCommandForm";
import UpdateCommandActions from "components/UpdateCommandActions";
import { useRouter } from "next/router";
import { useState } from "react";

import Error from "components/Error";

import sleep from "utils/sleep";

type Props = {
  Command: Command;
};

const UpdateCommand = (props: Props) => {
  const { Command } = props;
  const router = useRouter();

  const [newTitle, setNewTitle] = useState(Command.title);
  const [newAuthor, setNewAuthor] = useState(Command.author);
  const [newContent, setNewContent] = useState(Command.content);
  const [loading, setLoading] = useState(false);
  const [errors, setErrors] = useState<Array<string>>([]);

  const handleModifyClick = async () => {
    setLoading(true);
    setErrors([]);

    const response = await fetch(`/api/commando/${Command.id}/`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/x-www-form-urlencoded",
      },
      body: new URLSearchParams({
        title: newTitle,
        author: newAuthor,
        content: newContent,
      }),
    });

    await sleep(250);

    if (response.ok) {
      router.push(`/${Command.id}`);
    } else if (response.status === 422) {
      const errorsObj = await response.json();
      setErrors(Object.values(errorsObj));
    }

    setLoading(false);
  };

  return (
    <div className="flex-grow flex flex-col items-center">
      <UpdateCommandForm
        id={Command.id}
        title={Command.title}
        author={Command.author}
        content={Command.content}
        setNewTitle={setNewTitle}
        setNewAuthor={setNewAuthor}
        setNewContent={setNewContent}
      />
      {errors.length > 0 && <Error errors={errors} />}
      <UpdateCommandActions
        id={Command.id}
        handleModifyClick={handleModifyClick}
        loading={loading}
      />
    </div>
  );
};

export default UpdateCommand;
