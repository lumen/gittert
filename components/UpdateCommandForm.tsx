import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { useState } from "react";

type Props = {
  id: string;
  title: string;
  author: string;
  content: string;
  setNewTitle: React.Dispatch<React.SetStateAction<string>>;
  setNewAuthor: React.Dispatch<React.SetStateAction<string>>;
  setNewContent: React.Dispatch<React.SetStateAction<string>>;
};

const UpdateCommandForm = (props: Props) => {
  const {
    id,
    title,
    author,
    content,
    setNewTitle,
    setNewAuthor,
    setNewContent,
  } = props;

  return (
    <div className="px-24 py-4 w-[54rem]">
      <div className="form-control">
        <Link className="link link-hover text-sm w-fit mb-2 inline-flex gap-2 items-center" href={`/${id}`}>
            <FontAwesomeIcon icon={faAngleLeft} className="text-primary" />
           Return to Commando Overview
        </Link>
        <label className="label">
          <span className="label-text">Title</span>
        </label>
        <textarea
          className="textarea textarea-bordered bg-base-300 text-neutral-content"
          placeholder="Commando Title"
          defaultValue={title}
          onChange={(event) => setNewTitle(event.target.value)}
          rows={1}
        />
        <label className="label">
          <span className="label-text">Author</span>
        </label>
        <input
          className="input input-bordered bg-base-300 text-neutral-content"
          placeholder="Auteur"
          defaultValue={author}
          onChange={(event) => setNewAuthor(event.target.value)}
        />
        <label className="label">
          <span className="label-text">Content</span>
        </label>
        <textarea
          className="textarea textarea-bordered bg-base-300 text-neutral-content"
          placeholder="Description"
          defaultValue={content}
          onChange={(event) => setNewContent(event.target.value)}
          rows={10}
        />
      </div>
    </div>
  );
};

export default UpdateCommandForm;
