import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlus } from "@fortawesome/free-solid-svg-icons";
import Link from "next/link";
import { useEffect, useState } from "react";
import { useRouter } from "next/router";

import CommandPreview from "components/CommandPreview";
import Loader from "components/Loader";

import sleep from "utils/sleep";

const CommandList = () => {
  const router = useRouter();
  const [Commands, setCommands] = useState<Array<Command>>([]);

  useEffect(() => {
    const fetchCommands = async () => {
      const response = await fetch("/api/commando", {
        method: "GET",
      });

      await sleep(250);

      if (response.ok) {
        const data: Array<Command> = await response.json();
        setCommands(data);
      } else if (response.status === 401) {
        router.push('/auth/logout');
      }
    };

    fetchCommands();
  }, []);

  if (Commands.length === 0) {
    return <Loader />;
  }

  return (
    <div className="min-h-screen p-24 flex flex-col gap-6 animate-fade-in-up items-center">
      <Link className="w-fit" href="/create">
          <button className="btn gap-2">
            <FontAwesomeIcon icon={faPlus} className="text-primary" />
            Manual Commando
          </button>
      </Link>
      {Commands.map((Command) => (
        <CommandPreview key={Command.id} Command={Command} />
      ))}
    </div>
  );
};

export default CommandList;
