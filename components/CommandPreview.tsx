import Link from "next/link";

type Props = {
  Command: Command;
};

const CommandPreview = (props: Props) => {
  const { Command } = props;

  return (
    <Link href={Command.id}>
        <div className="card bg-base-300 w-[36rem] text-base-content">
          <div className="card-body">
            <h2 className="card-title">{Command.title}</h2>
            <p>{Command.content}</p>
            <p className="ml-auto">{Command.author}</p>
          </div>
        </div>
    </Link>
  );
};

export default CommandPreview;
