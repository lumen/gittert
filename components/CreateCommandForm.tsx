import { faAngleLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";

type Props = {
  setTitle: React.Dispatch<React.SetStateAction<string>>;
  setAuthor: React.Dispatch<React.SetStateAction<string>>;
  setContent: React.Dispatch<React.SetStateAction<string>>;
};

const CreateCommandForm = (props: Props) => {
  const { setTitle, setAuthor, setContent } = props;

  return (
    <div className="px-24 py-4 w-[54rem]">
      <div className="form-control">
        <Link className="link link-hover text-sm w-fit mb-2 inline-flex gap-2 items-center" href={`/`}>
            <FontAwesomeIcon icon={faAngleLeft} className="text-primary" />
            Back to Commando Overivew
        </Link>
        <label className="label">
          <span className="label-text">Title</span>
        </label>
        <textarea
          className="textarea textarea-bordered bg-base-300 text-neutral-content"
          placeholder="Title"
          onChange={(event) => setTitle(event.target.value)}
          rows={1}
        />
        <label className="label">
          <span className="label-text">Created By</span>
        </label>
        <input
          className="input input-bordered bg-base-300 text-neutral-content"
          placeholder="Author"
          onChange={(event) => setAuthor(event.target.value)}
        />
        <label className="label">
          <span className="label-text">Description</span>
        </label>
        <textarea
          className="textarea textarea-bordered bg-base-300 text-neutral-content"
          placeholder="Commando Description.."
          onChange={(event) => setContent(event.target.value)}
          rows={10}
        />
      </div>
    </div>
  );
};

export default CreateCommandForm;
