import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Link from "next/link";
import { faUser } from "@fortawesome/free-solid-svg-icons";
import { faUserPlus } from "@fortawesome/free-solid-svg-icons";
import { faDiceSix } from "@fortawesome/free-solid-svg-icons";

import Loader from "components/Loader";

import sleep from "utils/sleep";

const AuthPage: NextPage = () => {

  const router = useRouter();
  const { id } = router.query;

  return (
    <div className="flex-grow py-24 flex flex-col gap-12 justify-between items-center">
      <Link href="/auth/login">
      <div className="card bg-base-300 w-[36rem] text-base-content">
        <div className="card-body">
          <h2 className="card-title">
          <FontAwesomeIcon icon={faUser} className="text-primary" />
            Login
          </h2>
          <p>Login to your existing account.</p>
          <p className="ml-auto">/auth/login</p>
        </div>
      </div>
      </Link>
      <Link href="/auth/register">
          <div className="card bg-base-300 w-[36rem] text-base-content">
            <div className="card-body">
              <h2 className="card-title">
              <FontAwesomeIcon icon={faUserPlus} className="text-primary" />
                Register
              </h2>
              <p>Register a new account.</p>
              <p className="ml-auto">/auth/register</p>
            </div>
          </div>
      </Link>
      <Link href="/auth/register">
          <div className="card bg-base-300 w-[36rem] text-base-content">
            <div className="card-body">
              <h2 className="card-title">
              <FontAwesomeIcon icon={faDiceSix} className="text-primary mask-triangle-4" />
                All Games
              </h2>
              <p>View all games offered within this gateway.</p>
              <p className="ml-auto">/auth/register</p>
            </div>
          </div>
      </Link>

    </div>
  );
};

const getServerSideProps = async () => {
  return {
    props: {},
  };
};

export default AuthPage;
export { getServerSideProps };
