import { NextPage } from "next";

import Login from "components/auth/Login";

const LoginPage: NextPage = () => {
  return (
    <div className="min-h-screen flex flex-col justify-between animate-fade-in-up">
      <Login />
    </div>
  );
};

export default LoginPage;
