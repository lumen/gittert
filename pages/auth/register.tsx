import { NextPage } from "next";

import Register from "components/auth/Register";

const RegisterPage: NextPage = () => {
  return (
    <div className="min-h-screen flex flex-col justify-between animate-fade-in-up">
      <Register />
    </div>
  );
};

export default RegisterPage;
