import type { NextPage } from "next";

import CommandList from "components/CommandList";

const CommandsPage: NextPage = () => {
  return (
    <div>
      <CommandList />
    </div>
  );
};

export default CommandsPage;
