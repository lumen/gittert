import { NextPage } from "next";

import Signup from "components/Signup";

const SignupPage: NextPage = () => {
  return (
    <div className="min-h-screen flex flex-col justify-between animate-fade-in-up">
      <Signup />
    </div>
  );
};

export default SignupPage;
