import { NextPage } from "next";
import { useRouter } from "next/router";
import { useEffect, useState } from "react";

import Commando from "components/Commando";
import Loader from "components/Loader";

import sleep from "utils/sleep";

const CommandPage: NextPage = () => {

  const router = useRouter();
  const { id } = router.query;

  const [Command, setCommand] = useState<Command | null>(null);

  useEffect(() => {
    const fetchCommand = async () => {
      const response = await fetch(`/api/commando/${id}`, {
        method: "GET",
      });

      await sleep(250);

      if (response.ok) {
        const data: Command = await response.json();
        setCommand(data);
      }
    };

    fetchCommand();
  }, []);

  if (!Command) {
    return <Loader />;
  }

  return (
    <div className="min-h-screen flex flex-col justify-between animate-fade-in-up">
      <Commando Command={Command} />
    </div>
  );
};

const getServerSideProps = async () => {
  return {
    props: {},
  };
};

export default CommandPage;
export { getServerSideProps };
